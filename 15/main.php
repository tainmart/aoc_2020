<?php
ini_set('memory_limit', '-1');

$inputs = [7, 14, 0, 17, 11, 1, 2];

$solution1 = countMemoryUntil($inputs, 2020);
$solution2 = countMemoryUntil($inputs, 30000000);

echo "Solution Day 15-1: $solution1\n";
echo "Solution Day 15-2: $solution2\n";

function countMemoryUntil(array $inputs, int $until) :int
{
    $memory = array_combine($inputs, range(1, count($inputs)));
    $last = end($inputs);

    for ($turn = count($inputs); $turn < $until; $turn += 1) {
        if (!array_key_exists($last, $memory)) {
            $memory[$last] = $turn;
            $last = 0;
            continue;
        } 
        $previousTurn = $memory[$last];
        $memory[$last] = $turn;
        $last = $turn - $previousTurn;
    }

    return $last;
}
