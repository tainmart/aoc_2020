<?php

$inputs = file('input.txt', FILE_IGNORE_NEW_LINES);

$solution1 = do_my_homework($inputs, 'solve_simple');
$solution2 = do_my_homework($inputs, 'solve_advanced');

echo "Solution Day 18-1: $solution1\n";
echo "Solution Day 18-2: $solution2\n";

function do_my_homework(array $inputs, string $function) {
    $result = 0;
    foreach($inputs as $line) {
        // clear all ()
        while (preg_match('#\(([\d+* ]+)\)#', $line, $matches)) {
            $value = call_user_func($function, $matches[1]);
            $line = str_replace($matches[0], $value, $line);
        }
        $result += call_user_func($function, $line);
    }
    return $result;
}

function solve_simple(string $line) {
    preg_match_all('#(\d+|\+|\*)#', $line, $operands);
    $result = 0;
    $operator = '+';
    foreach($operands[1] as $operand) {
        if (is_numeric($operand)) {
            $result = $operator === '+' ? ($result + $operand) : ($result * $operand);
        } else {
            $operator = $operand;
        }
    }
    return $result;
}

function solve_advanced(string $line) {
    while(preg_match('#\d+ \+ \d+#', $line, $matches)) {
        $value = solve_simple($matches[0]);
        $line = substr_replace($line, $value, strpos($line, $matches[0]), strlen($matches[0]));
    }
    while(preg_match('#\d+ \* \d+#', $line, $matches)) {
        $value = solve_simple($matches[0]);
        $line = substr_replace($line, $value, strpos($line, $matches[0]), strlen($matches[0]));
    }
    return $line;
}
