<?php

const ITERATIONS = 6;

$inputs = array_map('str_split', array_map('trim', file('input.txt')));
$size = count($inputs) + 4 * ITERATIONS;
$start_x = $start_y = ($size - count($inputs)) / 2;
$start_z = $start_w = $size / 2;

$grid = array_fill(0, $size, array_fill(0, $size, array_fill(0, $size, false)));
foreach($inputs as $row => $line) {
    foreach($line as $column => $char) {
        $grid[$start_z][$start_y + $row][$start_x + $column] = $char === '#';
    }
}

for($_ = 0; $_ < ITERATIONS; $_ += 1) {
    $new_grid = $grid;
    for ($z = 1; $z < $size - 1; $z += 1) {
        for ($y = 1; $y < $size - 1; $y += 1) {
            for ($x = 1; $x < $size - 1; $x += 1) {
                $active_neighbours = 0;
                for ($dz = -1; $dz <= 1; $dz += 1) {
                    for ($dy = -1; $dy <= 1; $dy += 1) {
                        for ($dx = -1; $dx <= 1; $dx += 1) {
                            if ($dx === 0 && $dy === 0 && $dz === 0) {
                                continue;
                            }
                            $active_neighbours += $grid[$z+$dz][$y+$dy][$x+$dx] ? 1 : 0;
                        }
                    }
                }
                if ($grid[$z][$y][$x] && ($active_neighbours < 2 || $active_neighbours > 3)) {
                    $new_grid[$z][$y][$x] = false;
                }
                if (!$grid[$z][$y][$x] && $active_neighbours === 3) {
                    $new_grid[$z][$y][$x] = true;
                }
            }
        }
    }
    $grid = $new_grid;
}
$solution1 = 0;
for ($z = 0; $z < $size; $z += 1) {
    for ($y = 0; $y < $size; $y += 1) {
        for ($x = 0; $x < $size; $x += 1) {
            $solution1 += $grid[$z][$y][$x] ? 1 : 0;
        }
    }
}

// Spielt den selben songen nochmal!
$grid = array_fill(0, $size, array_fill(0, $size, array_fill(0, $size, array_fill(0, $size, false))));
foreach($inputs as $row => $line) {
    foreach($line as $column => $char) {
        $grid[$start_w][$start_z][$start_y + $row][$start_x + $column] = $char === '#';
    }
}

for($_ = 0; $_ < ITERATIONS; $_ += 1) {
    $new_grid = $grid;
    for ($w = 1; $w < $size - 1; $w += 1) {
        for ($z = 1; $z < $size - 1; $z += 1) {
            for ($y = 1; $y < $size - 1; $y += 1) {
                for ($x = 1; $x < $size - 1; $x += 1) {
                    $active_neighbours = 0;
                    for ($dw = -1; $dw <= 1; $dw += 1) {
                        for ($dz = -1; $dz <= 1; $dz += 1) {
                            for ($dy = -1; $dy <= 1; $dy += 1) {
                                for ($dx = -1; $dx <= 1; $dx += 1) {
                                    if ($dx === 0 && $dy === 0 && $dz === 0 && $dw === 0) {
                                        continue;
                                    }
                                    $active_neighbours += $grid[$w+$dw][$z+$dz][$y+$dy][$x+$dx] ? 1 : 0;
                                }
                            }
                        }
                    }
                    if ($grid[$w][$z][$y][$x] && ($active_neighbours < 2 || $active_neighbours > 3)) {
                        $new_grid[$w][$z][$y][$x] = false;
                    }
                    if (!$grid[$w][$z][$y][$x] && $active_neighbours === 3) {
                        $new_grid[$w][$z][$y][$x] = true;
                    }
                }
            }
        }
    }
    $grid = $new_grid;
}
$solution2 = 0;
for ($w = 0; $w < $size; $w += 1) {
    for ($z = 0; $z < $size; $z += 1) {
        for ($y = 0; $y < $size; $y += 1) {
            for ($x = 0; $x < $size; $x += 1) {
                $solution2 += $grid[$w][$z][$y][$x] ? 1 : 0;
            }
        }
    }
}

echo "Solution Day 17-1: $solution1\n";
echo "Solution Day 17-2: $solution2\n";
