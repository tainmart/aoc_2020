<?php

$inputs = explode("\n\n", file_get_contents('input.txt'));

$inputs = array_map(
    function($input) {
        parse_str(strtr($input, ": \n", "=&&"), $pairs);
        return $pairs;
    }, 
    $inputs
);

$required = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'];
$solution1 = array_filter(
    $inputs,
    function($input) use ($required) {
        $missing_keys = array_diff($required, array_keys($input));
        $count_missing_keys = count($missing_keys);
        return $count_missing_keys === 0 || ($count_missing_keys === 1 && array_key_exists('cid', $missing_keys));
    }
);

$solution2 = array_filter(
    $solution1,
    function($input) {
        $byr = $input['byr'];
        if ($byr < 1920 || $byr > 2002) {
            return false;
        }
        $iyr = $input['iyr'];
        if ($iyr < 2010 || $iyr > 2020) {
            return false;
        }
        $eyr = $input['eyr'];
        if ($eyr < 2020 || $eyr > 2030) {
            return false;
        }
        if (preg_match('#(\d+)(in|cm)#', $input['hgt'], $matches) === 0) {
            return false;
        }
        [$_, $hgt, $hgt_type] = $matches;
        if (
            !($hgt_type === 'cm' && $hgt >= 150 && $hgt <= 193) &&
            !($hgt_type === 'in' && $hgt >= 59 && $hgt <= 76)
        ) {
            return false;
        }
        if (!preg_match('/^#([a-fA-F0-9]{6})/i', $input['hcl'])) {
            return false;
        }
        if (!in_array($input['ecl'], ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'])) {
            return false;
        }
        if (!(is_numeric($input['pid']) && strlen($input['pid']) === 9)) {
            return false;
        }

        return true;
    }
);

$solution1 = count($solution1);
$solution2 = count($solution2);

echo "Solution Day 04-1: $solution1\n";
echo "Solution Day 04-2: $solution2\n";
