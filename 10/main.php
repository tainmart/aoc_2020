<?php

$inputs = array_map('intval', file('input.txt'));
sort($inputs);
// add outlet
array_unshift($inputs, 0);
// add device
array_push($inputs, max($inputs) + 3);

$jolts = [ 0 => 0, 1 => 0, 2 => 0, 3 => 0, ];
for ($index = 0; $index < count($inputs) -1; $index += 1) {
    $difference = $inputs[$index + 1] - $inputs[$index];
    $jolts[$difference] += 1;
}
$solution1 = $jolts[1] * $jolts[3];

$combinations = array_fill(0, count($inputs), 0);
$combinations[0] = 1;
for ($outer_index = 0; $outer_index < count($inputs); $outer_index += 1) {
    for($inner_index = $outer_index + 1; $inner_index < count($inputs); $inner_index += 1) {
        if ($inputs[$inner_index]  - $inputs[$outer_index] > 3) {
            break;
        }
        $combinations[$inner_index] += $combinations[$outer_index];
    }
}
$solution2 = end($combinations);

echo "Solution Day 10-1: $solution1\n";
echo "Solution Day 10-2: $solution2\n";
