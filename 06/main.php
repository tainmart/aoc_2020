<?php

$inputs = explode("\n\n", file_get_contents('input.txt'));
// remove newline that is added to the last entry
$inputs[count($inputs) - 1] = substr_replace($inputs[count($inputs) - 1], "", -1);

$solution1 = array_reduce(
    $inputs, 
    function($result, $input) {
        $input = str_replace("\n", '', $input);
        return $result + count(count_chars($input, 1));
    },
    0
);

$solution2 = array_reduce(
    $inputs, 
    function($result, $input) {
        $input = explode("\n", $input);
        if (count($input) === 1) {
            return $result + strlen($input[0]);
        }

        $input = array_map(
            fn($line) => str_split($line, 1),
           $input 
        );
        $same_answers = array_intersect(...$input);

        return $result + count($same_answers);
    },
    0
);

echo "Solution Day 06-1: $solution1\n";
echo "Solution Day 06-2: $solution2\n";
