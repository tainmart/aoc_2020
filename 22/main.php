<?php

$inputs = file_get_contents('input.txt');
[$me, $crab] = array_map(fn($player) => explode("\n", $player), explode("\n\n", $inputs));
array_shift($me);
array_shift($crab);
array_pop($crab);
$og_me = array_map('intval', $me);
$og_crab = array_map('intval', $crab);

$me = $og_me;
$crab = $og_crab;
while (count($me) && count($crab)) {
    $me_vs_crab = [array_shift($me), array_shift($crab)];
    if ($me_vs_crab[0] > $me_vs_crab[1]) {
        $me = array_merge($me, $me_vs_crab);
    } else {
        $crab = array_merge($crab, array_reverse($me_vs_crab));
    }
}
$winner = [];
if (count($me)) {
    $winner = $me;
} else {
    $winner = $crab;
}
$solution1 = 0;
for($index = 0; $index < count($winner); $index += 1) {
    $solution1 += $winner[$index] * (count($winner) - $index);
}

[$_, $winner] = recursive_frenzy($og_me, $og_crab);

$solution2 = 0;
for($index = 0; $index < count($winner); $index += 1) {
    $solution2 += $winner[$index] * (count($winner) - $index);
}

echo "Solution Day 22-1: $solution1\n";
echo "Solution Day 22-2: $solution2\n";


function recursive_frenzy(array $me, array $crab): array
{
    $previously_on_recursive_frenzy = [];
    while(count($me) && count($crab)) {
        $current_cards = [$me, $crab];
        if (in_array($current_cards, $previously_on_recursive_frenzy)) {
            return [true, $me];
        }
    
        $previously_on_recursive_frenzy[] = $current_cards;
        $me_vs_crab = [array_shift($me), array_shift($crab)];
        $winner = $me_vs_crab[0] > $me_vs_crab[1];
        if (count($me) >= $me_vs_crab[0] && count($crab) >= $me_vs_crab[1]) {
            $sub_me = $me;
            $sub_crab = $crab;
            [$winner, $_] = recursive_frenzy(
                array_splice($sub_me, 0, $me_vs_crab[0]), 
                array_splice($sub_crab, 0, $me_vs_crab[1])
            );
        }
        if ($winner) {
            $me = array_merge($me, $me_vs_crab);
        } else {
            $crab = array_merge($crab, array_reverse($me_vs_crab));
        }
    }
    if (count($me)) {
        return [true, $me];
    } 
    return [false, $crab];
}
