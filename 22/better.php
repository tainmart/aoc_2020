<?php

$⌨️ = file_get_contents('input.txt');
[$🤡, $🦀] = array_map(fn($🎮) => explode("\n", $🎮), explode("\n\n", $⌨️));
array_shift($🤡);
array_shift($🦀);
array_pop($🦀);
$🤡™️ = array_map('intval', $🤡);
$🦀™️ = array_map('intval', $🦀);

$🤡 = $🤡™️ ;
$🦀 = $🦀™️ ;
while (count($🤡) && count($🦀)) {
    $🤡⚔️🦀 = [array_shift($🤡), array_shift($🦀)];
    if ($🤡⚔️🦀[0] > $🤡⚔️🦀[1]) {
        $🤡 = array_merge($🤡, $🤡⚔️🦀);
    } else {
        $🦀 = array_merge($🦀, array_reverse($🤡⚔️🦀));
    }
}
$🥇 = [];
if (count($🤡)) {
    $🥇 = $🤡;
} else {
    $🥇 = $🦀;
}
$🅰️ = 0;
for($ℹ = 0; $ℹ < count($🥇); $ℹ += 1) {
    $🅰️ += $🥇[$ℹ] * (count($🥇) - $ℹ);
}

[$_, $🥇] = 🎰($🤡™️, $🦀™️);

$🅱️ = 0;
for($ℹ = 0; $ℹ < count($🥇); $ℹ += 1) {
    $🅱️ += $🥇[$ℹ] * (count($🥇) - $ℹ);
}

echo "Solution Day 22-1: $🅰️\n";
echo "Solution Day 22-2: $🅱️\n";


function 🎰(array $🤡, array $🦀): array
{
    $🔁 = [];
    while(count($🤡) && count($🦀)) {
        $🃏 = [$🤡, $🦀];
        if (in_array($🃏, $🔁)) {
            return [true, $🤡];
        }
    
        $🔁[] = $🃏;
        $🤡⚔️🦀 = [array_shift($🤡), array_shift($🦀)];
        $🥇 = $🤡⚔️🦀[0] > $🤡⚔️🦀[1];
        if (count($🤡) >= $🤡⚔️🦀[0] && count($🦀) >= $🤡⚔️🦀[1]) {
            $🤡©️ = $🤡;
            $🦀©️ = $🦀;
            [$🥇, $_] = 🎰(
                array_splice($🤡©️, 0, $🤡⚔️🦀[0]), 
                array_splice($🦀©️, 0, $🤡⚔️🦀[1])
            );
        }
        if ($🥇) {
            $🤡 = array_merge($🤡, $🤡⚔️🦀);
        } else {
            $🦀 = array_merge($🦀, array_reverse($🤡⚔️🦀));
        }
    }
    if (count($🤡)) {
        return [true, $🤡];
    } 
    return [false, $🦀];
}
