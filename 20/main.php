<?php

$inputs = explode("\n\n", file_get_contents('input.txt'));
array_pop($inputs);

$tiles = [];
foreach($inputs as $line) {
    [$key, $image] = explode(":\n", $line);
    $tiles[explode(' ', $key)[1]] = array_map('str_split', explode("\n", $image));
}

$tile_options = array_map('all_possible_tiles', $tiles);
$all_borders = [];
foreach($tile_options as $tile_number => $option) {
    foreach($option as $orientation => $tile) {
        $borders = get_tile_borders($tile);
        $all_borders[$tile_number][$orientation] = $borders;
    }
}
$size = sqrt(count($tiles));
$empty_board = array_fill(0, $size, array_fill(0, $size, null));

$board = generate_board($all_borders, $empty_board, 0, 0, []);
$corners = [
    $board[0][0],
    $board[0][$size-1],
    $board[$size-1][0],
    $board[$size-1][$size-1],
];
$solution1 = array_reduce(
    $corners,
    static function ($result, $corner) {
        return $result * $corner[0];
    },
    1,
);

$image = create_image($board, $tile_options);
$image_options = all_possible_tiles($image);

$solution2 = 0;
foreach($image_options as $image_option) {
    $solution2 = i_am_scared_of_nessie($image_option);
    echo "$solution2\n";
    if ($solution2 !== 0) {
        break;
    }

}
echo "Solution Day 20-1: $solution1\n";
echo "Solution Day 20-2: $solution2\n";

function i_am_scared_of_nessie(array $image): int
{
    $nessie = [
        '                  # ',
        '#    ##    ##    ###',
        ' #  #  #  #  #  #   '
    ];
    $nessie = array_map('str_split', $nessie);

    $nessies_body = [];
    for($y = 0; $y < count($nessie); $y += 1){
        for($x = 0; $x < count($nessie[$y]); $x += 1){
            if ($nessie[$y][$x] === '#'){
                $nessies_body[] = [$x, $y];
            }
        }
    }

    $encountered_nessie = False;
    for($y = 0; $y < count($image) - count($nessie); $y += 1){
        for($x = 0; $x < count($image[$y]) - count($nessie[0]); $x += 1){
            $has_monster = true;
            foreach($nessies_body as $body_part) {
                [$nessie_x, $nessie_y] = $body_part;
                if ($image[$y+$nessie_y][$x+$nessie_x] !== '#') {
                    $has_monster = False;
                    break;
                }
            }
            if ($has_monster) {
                $encountered_nessie = True;
                foreach($nessies_body as $body_part) {
                    [$nessie_x, $nessie_y] = $body_part;
                    $image[$y+$nessie_y][$x+$nessie_x] = 'O';
                }
            }
        }
    }
    if (! $encountered_nessie) {
        return 0;
    }
    $count = 0;
    for($y = 0; $y < count($image); $y += 1){
        for($x = 0; $x < count($image[$y]); $x += 1){
            $count += $image[$y][$x] === '#' ? 1 : 0;
            $char = $image[$y][$x];
            echo "$char";
        }
        echo "\n";
    }
    return $count;
}

function create_image(array $board, array $tile_options): array {
    $image = [];
    foreach($board as $line) {
        $tiles = [];
        foreach($line as $tile_config) {
            [$tile_number, $tile_orientation] = $tile_config;
            $tile = $tile_options[$tile_number][$tile_orientation];
            array_pop($tile);
            array_shift($tile);
            $tile = array_map(static function ($line) { array_pop($line); array_shift($line); return $line; }, $tile);
            $tiles[] = $tile;
        }
        for ($y = 0; $y < count($tiles[0]); $y += 1) {
            $row = [];
            for ($index = 0; $index < count($tiles); $index += 1) {
                for ($x = 0; $x < count($tiles[0]); $x += 1) {
                    $row[] = $tiles[$index][$x][$y];
                }
            }
            $image[] = $row;
        }
    }
    return $image;
}

function generate_board(array $all_borders, array $board, int $x, int $y, array $seen): ?array
{
    $size = count($board);
    if ($y === $size) {
        return $board;
    }

    $next_x = $x + 1;
    $next_y = $y;
    if ($x === $size-1) {
        $next_x = 0;
        $next_y += 1;
    }

    foreach($all_borders as $tile_number => $borders) {
        if (in_array($tile_number, $seen)) {
            continue;
        } 
        $seen[] = $tile_number;
        foreach($borders as $orientation => $border) {
            [$top, $right, $bottom, $left] = $border;
            if ($x > 0) {
                [$neighbour_number, $neighbour_orientation] = $board[$x-1][$y];
                [$neighbour_top, $neighbour_right, $neighbour_bottom, $neighbour_left] = $all_borders[$neighbour_number][$neighbour_orientation];

                if ($neighbour_right !== $left) {
                    continue;
                }
            }
            if ($y > 0) {
                [$neighbour_number, $neighbour_orientation] = $board[$x][$y-1];
                [$neighbour_top, $neighbour_right, $neighbour_bottom, $neighbour_left] = $all_borders[$neighbour_number][$neighbour_orientation];
                if ($neighbour_bottom !== $top) {
                    continue;
                }
            }
            $board[$x][$y] = [$tile_number, $orientation];
            $new_board = generate_board($all_borders, $board, $next_x, $next_y, $seen);
            if ($new_board !== null) {
                return $new_board;
            }
        }
        $seen = array_diff($seen, [$tile_number]);
    }
    $board[$x][$y] = null;
    return null;
}

function get_tile_borders(array $tiles): array
{
    $top = reset($tiles);
    $right = array_column($tiles, count($tiles[0])-1);
    $bottom = end($tiles);
    $left = array_column($tiles, 0);

    return [$top, $right, $bottom, $left];

}

function all_possible_tiles(array $tiles): array
{
    $possible_tiles = [];
    foreach (mirror_tiles($tiles) as $mirrored_tile) {
        $possible_tiles = array_merge($possible_tiles, rotate_tiles($mirrored_tile));
    }
    $possible_tiles = array_unique($possible_tiles, SORT_REGULAR);

    return $possible_tiles;
}

function mirror_tiles(array $tiles): array
{
    $mirrored_tiles = [];
    // normal
    $mirrored_tiles[] = $tiles;
    // vertical
    $mirrored_tiles[] = array_reverse($tiles);
    // horizontal
    $mirrored_tiles[] = array_map('array_reverse', $tiles);
    // vertical & horizontal
    $mirrored_tiles[] = array_map('array_reverse', array_reverse($tiles));

    return $mirrored_tiles;
}

function rotate_tiles(array $tiles): array
{
    $rotated_tiles = [];
    $rotated_tiles[] = $tiles;
    $previous = $tiles;
    for($_ = 0; $_ < 3; $_ += 1) {
        for ($y = 0; $y < count($tiles); $y += 1) {
            for ($x = 0; $x < count($tiles[$y]); $x += 1) {
                $tiles[$y][$x] = $previous[count($tiles[$y])-1-$x][$y];
            }
        }
        $previous = $tiles;
        $rotated_tiles[] = $tiles;
    }

    return $rotated_tiles;
}
