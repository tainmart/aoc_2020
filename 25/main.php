<?php

const SUBJECT_NUMBER = 7;
const REMAINDER = 20201227;

$inputs = file('input.txt', FILE_IGNORE_NEW_LINES);
[$public_key_card, $public_key_door] = array_map('intval', $inputs);

$loop_size_card = get_loop_size($public_key_card);
$loop_size_door = get_loop_size($public_key_door);

$encryption_key_card = get_encryption_key($public_key_card, $loop_size_door);
$encryption_key_door = get_encryption_key($public_key_door, $loop_size_card);

if ($encryption_key_card !== $encryption_key_door) {
    echo "wrong encryption key!\n";
}
$solution1 = $encryption_key_card;

echo "Solution Day 25-1: $solution1\n";
echo "Solution Day 25-2: Thank you reindeer\n";

function get_loop_size(int $public_key): int
{
    $loop_size = 0;
    $current_number = 1;

    while ($current_number !== $public_key) {
        $loop_size += 1;
        $current_number *= SUBJECT_NUMBER;
        $current_number %= REMAINDER;
    }
    return $loop_size;
}

function get_encryption_key(int $public_key, int $loop_size): int
{
    $current_number = 1;
    for ($_ = 0; $_ < $loop_size; $_ += 1) {
        $current_number *= $public_key;
        $current_number %= REMAINDER;
    }
    return $current_number;
}
