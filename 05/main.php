<?php

$inputs = file('input.txt');

$seatIds = array_map(
    function($input) {
        $binary = str_split(strtr($input, 'FBLR', '0101'), 7);
        [$row, $column] = array_map(fn($number) => bindec($number), $binary);
        return $row * 8 + $column;
    },
    $inputs
);
$remainingSeat = array_diff(range(min($seatIds), max($seatIds)), $seatIds);

$solution1 = max($seatIds);
$solution2 = reset($remainingSeat);

echo "Solution Day 05-1: $solution1\n";
echo "Solution Day 05-2: $solution2\n";
