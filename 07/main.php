<?php

$inputs = file('input.txt');
$bags = [];
$contained_bags = [];
foreach($inputs as $line) {
    preg_match('#(.+?) bags contain#', $line, $outer_match);
    preg_match_all('#(\d+) (.+?) bags?[,.]#', $line, $inner_matches);
    $key = $outer_match[1];

    $values = array_combine($inner_matches[2], $inner_matches[1]);
    $bags[$key] = $values;
    foreach($values as $color => $amount) {
        $contained_bags[$color][] = $key;
    }
}

$solution1 = count(contained_colors('shiny gold', $contained_bags));
$solution2 = amount_bags('shiny gold', $bags);

echo "Solution Day 07-1: $solution1\n";
echo "Solution Day 07-2: $solution2\n";


function contained_colors(string $color, array $contained) {
    $allBags = [];
    if (!array_key_exists($color, $contained)) {
        return $allBags;
    }
    
    foreach($contained[$color] as $bag_color) {
        $allBags[] = $bag_color;
        $allBags = array_merge(contained_colors($bag_color, $contained), $allBags);
    }
    return array_unique($allBags);
}

function amount_bags(string $color, array $bags) {
    $count = 0;
    foreach($bags[$color] as $bag_color => $amount) {
        $count += $amount;
        $count += $amount * amount_bags($bag_color, $bags);
    }
    return $count;
}
