<?php

$solution1 = 0;
$solution2 = 0;

$inputs = file('input.txt');
foreach($inputs as $line) {
    [$numbers, $char, $password] = explode(" ", $line);
    [$min, $max] = array_map('intval', explode("-", $numbers));
    $char = $char[0];

    $countChar = substr_count($password, $char);
    $solution1 += $countChar >= $min && $countChar <= $max ? 1 : 0;

    $solution2 += ($password[$min-1] === $char) ^ ($password[$max-1] === $char) ? 1 : 0;
}
echo "Solution Day 02-1: $solution1\n";
echo "Solution Day 02-2: $solution2\n";
