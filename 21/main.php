<?php

$inputs = file('input.txt', FILE_IGNORE_NEW_LINES);

$ingredients_count = [];
$unique_ingredients = [];
$possible_allergenes = [];

foreach($inputs as $line) {
    [$ingredients, $allergens] = explode(' (contains ', $line);
    $ingredients = explode(' ', $ingredients);
    $allergens = explode(', ', str_replace(')', '',  $allergens));

    foreach($ingredients as $ingredient) {
        $unique_ingredients[] = $ingredient;
        if (! isset($ingredients_count[$ingredient])) {
            $ingredients_count[$ingredient] = 0;
        }
        $ingredients_count[$ingredient] += 1;
    }
    $unique_ingredients = array_unique($unique_ingredients);

    foreach($allergens as $allergen) {
        if (! isset($possible_allergenes[$allergen])) {
            $possible_allergenes[$allergen] = $ingredients;
        } else {
            $possible_allergenes[$allergen] = array_intersect($possible_allergenes[$allergen], $ingredients);
        }
    }
}

$this_i_can_eat = [];
foreach ($unique_ingredients as $ingredient) {
    $this_i_can_eat[] = $ingredient;
    foreach($possible_allergenes as $allergen => $ingredient_list) {
        if (in_array($ingredient, $ingredient_list)) {
            $this_i_can_eat = array_diff($this_i_can_eat, [$ingredient]);
            break;
        }
    }
}
$solution1 = 0;
foreach($this_i_can_eat as $nom_nom_nom) {
    $solution1 += $ingredients_count[$nom_nom_nom];
}

$resolved_allergerns = [];
$resolved_ingredients = [];

while (count($resolved_allergerns) < count($possible_allergenes)) {
    foreach(array_keys($possible_allergenes) as $allergen) {
        if (in_array($allergen, $resolved_allergerns)) {
            continue;
        }
        foreach($resolved_ingredients as $resolved_ingredient) {
            if (in_array($resolved_ingredient, $possible_allergenes[$allergen])) {
                $possible_allergenes[$allergen] = array_diff($possible_allergenes[$allergen], [$resolved_ingredient]);
            }
        }
        if (count($possible_allergenes[$allergen]) === 1) {
            $resolved_allergerns[] = $allergen;
            foreach($possible_allergenes[$allergen] as $ingredient) {
                $resolved_ingredients[] = $ingredient;
            }
        }
    }
}
ksort($possible_allergenes);
$solution2 = implode(',', array_map(fn($line) => reset($line), $possible_allergenes));

echo "Solution Day 21-1: $solution1\n";
echo "Solution Day 21-2: $solution2\n";
