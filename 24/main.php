<?php

$inputs = file('input.txt', FILE_IGNORE_NEW_LINES);

$directions = [
    'e'  => [ 1, 0],
    'se' => [ 0,-1],
    'sw' => [-1,-1],
    'w'  => [-1, 0],
    'nw' => [ 0, 1],
    'ne' => [ 1, 1],
];

$tiles = [];
foreach($inputs as $line) {
    $instructions = preg_match_all('#e|se|sw|w|nw|ne#', $line, $matches);
    $x = $y = 0;
    foreach($matches[0] as $instruction) {
        $x += $directions[$instruction][0];
        $y += $directions[$instruction][1];
    }
    if (!isset($tiles[$x]) || !isset($tiles[$x][$y])) {
        $tiles[$x][$y] = 0;
    } 
    $tiles[$x][$y] = ($tiles[$x][$y] + 1) % 2;
}
$solution1 = count_tiles($tiles);

for ($_ = 0; $_ < 100; $_ += 1) {
    foreach ($tiles as $x => $tile_row) {
        foreach ($tile_row as $y => $tile_value) {
            foreach ($directions as $direction) {
                $new_x = $x + $direction[0];
                $new_y = $y + $direction[1];
                if (!isset($tiles[$new_x]) || !isset($tiles[$new_x][$new_y])) {
                    $tiles[$new_x][$new_y] = 0;
                }
            }
        }
    }

    $new_tiles = $tiles;
    foreach ($tiles as $x => $tile_row) {
        foreach ($tile_row as $y => $tile_value) {
            $count_flipped_neighbours = 0;
            foreach ($directions as $direction) {
                $neighbour_x = $x + $direction[0];
                $neighbour_y = $y + $direction[1];
                if (!isset($tiles[$neighbour_x]) || !isset($tiles[$neighbour_x][$neighbour_y])) {
                    continue;
                }
                $count_flipped_neighbours += $tiles[$neighbour_x][$neighbour_y];
            }

            if (($tile_value === 0 && $count_flipped_neighbours === 2) ||
                ($tile_value === 1 && $count_flipped_neighbours === 0) ||
                ($tile_value === 1 && $count_flipped_neighbours > 2 )) {
                $new_tiles[$x][$y] += 1;
            }
            $new_tiles[$x][$y] %= 2;
        }
    }
    $tiles = $new_tiles;
}

$solution2 = count_tiles($tiles);

echo "Solution Day 24-1: $solution1\n";
echo "Solution Day 24-2: $solution2\n";

function count_tiles(array $tiles): int
{
    return array_reduce(
        $tiles,
        static function ($result, $row) {
            $tile_overview = array_count_values($row);
            if (!isset($tile_overview[1])) {
                return $result;
            }
            return $result + $tile_overview[1];
        },
        0
    );
}
