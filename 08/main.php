<?php

$inputs = file('input.txt');

[$exit_code, $solution1] = run_intcode($inputs);

$solution2 = 0;
foreach ($inputs as $index => $line) {
    [$command, $value] = explode(' ', $inputs[$index]);
    if ($command === 'acc') {
        continue;
    }
    $changed_inputs = $inputs;
    $changed_inputs[$index] = strtr($changed_inputs[$index], ['jmp' => 'nop', 'nop' => 'jmp']);
    
    [$exit_code, $solution2] = run_intcode($changed_inputs);
    if ($exit_code === 0) {
        break;
    }

}

echo "Solution Day 08-1: $solution1\n";
echo "Solution Day 08-2: $solution2\n";


function run_intcode($inputs): array
{
    $index = 0;
    $accumulator = 0;
    $visited = [];
    while ($index < count($inputs)) {
        if (in_array($index, $visited)) {
            return [1, $accumulator];
        }
        $visited[] = $index;
        [$command, $value] = explode(' ', $inputs[$index]);
        $value = (int)$value;
        switch ($command) {
            case 'jmp':
                $index += $value;
                break;
            case 'acc':
                $index += 1;
                $accumulator += $value;
                break;
            case 'nop':
                $index += 1;
                break;
            default:
                throw new Error('Unexpected IntCode');
        }
    }
    
    return [0, $accumulator];
}
