<?php

$inputs = array_map('trim', file('input.txt'));

$solution1 = 0;
$mask = 0;
$memory = [];
foreach($inputs as $line) {
    [$command, $value] = explode(" = ", $line);
    if ($command === "mask") {
        $mask = $value;
    } else {
        preg_match('#\[([^\]]*)\]#', $command, $matches);
        $address = (int)$matches[1];
        $value |= bindec(strtr($mask, 'X', '0'));
        $value &= bindec(strtr($mask, 'X', '1'));
        $memory[$address] = $value;
    }

}
$solution1 = array_sum($memory);

$solution2 = 0;
$mask = 0;
$memory = [];
foreach($inputs as $line) {
    [$command, $value] = explode(" = ", $line);
    if ($command === "mask") {
        $mask = $value;
    } else {
        preg_match('#\[([^\]]*)\]#', $command, $matches);
        $address = (int)$matches[1];
        $address |= bindec(strtr($mask, 'X', '0'));
        foreach(all_addresses($address, $mask) as $new_address) {
            $memory[$new_address] = $value;
        }
    }
}
$solution2 = array_sum($memory);

echo "Solution Day 14-1: $solution1\n";
echo "Solution Day 14-2: $solution2\n";

function all_addresses($address, $mask): array
{
    $binary_address = str_pad(decbin($address), strlen($mask), '0', STR_PAD_LEFT);
    $addresses = [$binary_address];

    for ($index = 0; $index < strlen($mask); $index += 1) {
        if ($mask[$index] === 'X') {
            $addresses_to_add = [];
            foreach($addresses as $new_address) {
                $new_address[$index] = '0';
                $addresses_to_add[] = $new_address;
                $new_address[$index] = '1';
                $addresses_to_add[] = $new_address;
            }
            $addresses = $addresses_to_add;
        }
    }

    return $addresses;
}
