<?php

$inputs = explode("\n\n", file_get_contents('input.txt'));
// $inputs = explode("\n\n", file_get_contents('test.txt'));
$rules = [];
foreach(explode("\n", $inputs[0]) as $rule) {
    [$number, $instruction] = explode(': ', $rule);
    $rules[$number] = $instruction;
}
$messages = explode("\n", $inputs[1]);
array_pop($messages);

$regex =  '#^' . create_regex(0, $rules) . '$#';
$solution1 = 0;
foreach($messages as $message) {
    $solution1 += preg_match($regex, $message) ? 1 : 0;
}

$rules[8] = '42 | 42 8';
$rules[11] = '42 31 | 42 11 31';
$regex =  '#^' . create_regex(0, $rules, true) . '$#';
$solution2 = 0;
foreach($messages as $message) {
    $solution2 += preg_match($regex, $message) ? 1 : 0;
}

echo "Solution Day 19-1: $solution1\n";
echo "Solution Day 19-2: $solution2\n";

function create_regex(int $rule_number, array $rules, bool $part2 = false): string {
    $rule = $rules[$rule_number];
    if (preg_match('#"([a|b])"#', $rule, $matches)) {
        return $matches[1];
    }

    if ($part2) {
        if ($rule_number === 8) {
            return create_regex('42', $rules, true) . '+';
        }
        if ($rule_number === 11) {
            $rule_42 = create_regex('42', $rules, true);
            $rule_31 = create_regex('31', $rules, true);
            $regex = '(';
            $repeatings = [];
            for($repeat = 1; $repeat < 5; $repeat += 1) {
                $repeatings[] = sprintf('(%s{%s}%s{%s})', $rule_42, $repeat, $rule_31, $repeat);
            }
            $regex .= implode("|", $repeatings);
            $regex .= ')';
            return $regex;
        }
    }

    $regex = '(';
    foreach(explode(' ', $rule) as $element) {
        if ($element === '|') {
            $regex .= $element;
        } else {
            $regex .= create_regex($element, $rules, $part2);
        }
    }
    $regex .= ')';
    return $regex;
}
