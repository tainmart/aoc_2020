<?php

$inputs = file('input.txt');
$arrival = (int)$inputs[0];
$bus_times = array_map(
     function($entry) { return is_numeric(trim($entry)) ? (int)$entry : $entry; },
     explode(',', $inputs[1])
);
$bus_times = array_filter($bus_times, 'is_numeric');

$min_wait = $arrival;
$earliest = $bus_times[0];
foreach($bus_times as $bus_time) {
    $to_wait = $bus_time - $arrival % $bus_time;
    if ($to_wait < $min_wait) {
        $min_wait = $to_wait;
        $earliest = $bus_time;
    }
}
$solution1 = $earliest * $min_wait;


$position = 0;
$increment = $bus_times[0];
foreach($bus_times as $bus_offset => $bus_time) {
    while (($position + $bus_offset) % $bus_time) {
        $position += $increment;
    }
    $increment = least_common_multiplier($increment, $bus_time);
}
$solution2 = $position;

echo "Solution Day 13-1: $solution1\n";
echo "Solution Day 13-2: $solution2\n";


function least_common_multiplier($a, $b) {
    return ($a * $b) / greastest_common_divisor($a, $b);
}

function greastest_common_divisor($a, $b) {
    if ($b > 0) {
        return greastest_common_divisor($b, $a % $b);
    }
    return $a;
}
