<?php

const O_N_E__M_I_L_L_I_O_N = 1000000; 
const T_E_N__M_I_L_L_I_O_N = 10000000; 

$inputs = array_map('intval', str_split(trim(file_get_contents('input.txt'))));

$cups = crabby_function($inputs, 100);
array_shift($cups);
$solution1 = implode('', $cups);

$cups = crabby_function(array_merge($inputs, range(max($inputs) + 1, O_N_E__M_I_L_L_I_O_N)), T_E_N__M_I_L_L_I_O_N);
$solution2 = $cups[1] * $cups[2];

echo "Solution Day 23-1: $solution1\n";
echo "Solution Day 23-2: $solution2\n";

function crabby_function(array $cups, int $loops): array {
    $linked_list = [];
    for ($index = 0; $index < count($cups) - 1; $index += 1) {
        $linked_list[$cups[$index]] = $cups[$index + 1];
    }
    $linked_list[$cups[count($cups)-1]] = $cups[0];
    
    $current_cup = $cups[0];
    $max = max($cups);
    for ($_ = 0; $_ < $loops; $_ += 1) {
        if ($_ % ($loops / 100) === 0) {
            $percentage = $_ / $loops * 100;
            printf("%s%s\r", str_repeat('#', $percentage), str_repeat('-', 100 - $percentage));
        }

        $pointer = $linked_list[$current_cup];
        $moving_cups = [];
        for($__ = 0; $__ < 3; $__ += 1) {
            $moving_cups[] = $pointer;
            $pointer = $linked_list[$pointer];
        }
        $destination_cup = $current_cup;
        while($destination_cup === $current_cup || in_array($destination_cup, $moving_cups)) {
            $destination_cup -= 1;
            if ($destination_cup == 0) {
                $destination_cup = $max;
            }
        }
        $next_cup = $linked_list[$destination_cup];

        $linked_list[$destination_cup] = $moving_cups[0];
        $linked_list[$moving_cups[count($moving_cups) - 1]] = $next_cup;
        $linked_list[$current_cup] = $pointer;

        $current_cup = $pointer;
    }
    printf("%s\r", str_repeat(' ', 100));

    $new_cups = [];
    $pointer = array_search(1, $linked_list);
    for ($i = 0; $i < count($linked_list); $i += 1) {
        $new_cups[] = $linked_list[$pointer];
        $pointer = $linked_list[$pointer];
    }

    return $new_cups;
}

function what_a_crab(array $cups, int $loops): array {
    $max = max($cups);
    for ($_ = 0; $_ < $loops; $_ += 1) {
        $moving_cups = array_splice($cups, 1, 3);
        $destination_cup = $cups[0];
        while ($destination_cup === $cups[0] || in_array($destination_cup, $moving_cups)) {
            $destination_cup -= 1;
            if ($destination_cup == 0) {
                $destination_cup = $max;
            }
        }
        $new_position = array_search($destination_cup, $cups) + 1;
        
        $cups = array_merge(
            array_slice($cups, 0, $new_position),
            $moving_cups,
            array_slice($cups, $new_position)
        );

        $cups = array_merge(
            array_slice($cups, 1),
            array_slice($cups, 0, 1)
        );
    }

    $number_1_cup_position = array_search(1, $cups);
    $cups = array_merge(
        array_slice($cups, $number_1_cup_position),
        array_slice($cups, 0, $number_1_cup_position)
    );

    return $cups;
}
