<?php

$inputs = array_map('intval', file('input.txt'));
$solution1 = 0;
$solution2 = 0;
foreach ($inputs as $number1) {
    foreach ($inputs as $number2) {
        if ($number1 + $number2 === 2020) {
            $solution1 = $number1 * $number2; 
        }
        foreach ($inputs as $number3) {
            if ($number1 + $number2 + $number3 === 2020) {
                $solution2 = $number1 * $number2 * $number3; 
            }
        }
    }
}
echo "Solution Day 01-1: $solution1\n";
echo "Solution Day 01-2: $solution2\n";
