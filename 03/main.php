<?php

$solution1 = 1;
$solution2 = 1;

$inputs = array_map('trim', file('input.txt'));
$slopes1 = [[1,2]];
$slopes2 = [[1,1], [3,1], [5,1], [7,1], [1,2]];

foreach($slopes1 as $slope) {
    $solution1 *= traverseSlope($slope[0], $slope[1], $inputs);
}
foreach($slopes2 as $slope) {
    $solution2 *= traverseSlope($slope[0], $slope[1], $inputs);
}
echo "Solution Day 03-1: $solution1\n";
echo "Solution Day 03-2: $solution2\n";

function traverseSlope(int $right, int $down, array $lines): int
{
    $total = 0;
    $x = $y = 0;
    $lineLength = strlen($lines[0]);

    while($y < count($lines)) {
        $total += $lines[$y][$x] === '#' ? 1 : 0;
        $x = ($x + $right) % $lineLength;
        $y += $down;
    }
    return $total;
}
