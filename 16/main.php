<?php

// === start parsing ===
$inputs = explode("\n\n", file_get_contents('input.txt'));
$rules = [];
foreach(explode("\n", $inputs[0]) as $rule) {
    preg_match("#(.*): (\d+-\d+) .* (\d+-\d+)#", $rule, $matches);
    [$_, $rule, $first_range, $second_range] = $matches;
    $rules[$rule] = [
        array_map('intval', explode('-', $first_range)),
        array_map('intval', explode('-', $second_range)),
    ];
}
$my_ticket = explode("\n", $inputs[1]);
$my_ticket = array_map('intval', explode(',', $my_ticket[1]));

$nearby_tickets = explode("\n", $inputs[2]);
array_shift($nearby_tickets);
array_pop($nearby_tickets);
$nearby_tickets = array_map(fn($ticket) => array_map('intval', explode(',', $ticket)), $nearby_tickets);
// === end parsing ===

$solution1 = array_reduce(
    $nearby_tickets,
    static function (int $result, array $ticket) use ($rules) {
        $invalidNumbers = array_filter(
            $ticket,
            static function ($ticket_number) use ($rules) {
                foreach ($rules as $rule) {
                    if (($rule[0][0] <= $ticket_number && $ticket_number <= $rule[0][1]) ||
                        ($rule[1][0] <= $ticket_number && $ticket_number <= $rule[1][1])) {
                        return false;

                    }
                }
                return true;
            }
        );
        return $result + array_sum($invalidNumbers);
    },
    0
);

$nearby_tickets = array_filter(
    $nearby_tickets,
    static function (array $ticket) use ($rules) {
        $invalidNumbers = array_filter(
            $ticket,
            static function ($ticket_number) use ($rules) {
                foreach ($rules as $rule) {
                    if (($rule[0][0] <= $ticket_number && $ticket_number <= $rule[0][1]) ||
                        ($rule[1][0] <= $ticket_number && $ticket_number <= $rule[1][1])) {
                        return false;
                    }
                }
                return true;
            }
        );
        return empty($invalidNumbers);
    }
);

$rule_positions = array_fill(0, count($rules), array_flip(array_keys($rules)));
foreach($nearby_tickets as $ticket) {
    foreach($rules as $rule_name => $rule_ranges) {
        foreach($ticket as $index => $ticket_number) {
            if (!( ($rule_ranges[0][0] <= $ticket_number && $ticket_number <= $rule_ranges[0][1]) ||
                   ($rule_ranges[1][0] <= $ticket_number && $ticket_number <= $rule_ranges[1][1]) )) {
                unset($rule_positions[$index][$rule_name]);
            }
        }
    }
}
uasort(
    $rule_positions,
    static function ($first, $second) {
        return count($first) <=> count($second);
    }
);

$rules_order = [];
while (!empty($rule_positions)) {
    $position = key($rule_positions);
    $rule_name = key($rule_positions[$position]);
    unset($rule_positions[$position]);

    $rules_order[$rule_name] = $position;

    $rule_positions = array_map(
        static function($rule_position) use ($rule_name) {
            unset($rule_position[$rule_name]);
            return $rule_position;
        },
        $rule_positions
    );
}

$solution2 = 1;
foreach ($rules_order as $name => $pos) {
    if (substr($name, 0, 9) !== 'departure') {
        continue;
    }
    $solution2 *= $my_ticket[$pos];
}

echo "Solution Day 16-1: $solution1\n";
echo "Solution Day 16-2: $solution2\n";
