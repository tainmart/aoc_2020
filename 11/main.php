<?php

$inputs = array_map('str_split', file('input.txt'));

$solution1 = calculateSeating($inputs, 4, 'amountOccupiedNeighbours');
$solution2 = calculateSeating($inputs, 5, 'amountDirectionalNeighbours');

echo "Solution Day 11-1: $solution1\n";
echo "Solution Day 11-2: $solution2\n";


function calculateSeating(array $grid, int $tolerance, string $functionName) {
    $height = count($grid);
    $width = count ($grid[0]);

    $previousSeating = $grid;
    $seating = [];
    while ($previousSeating !== $seating) {
        $seating = $previousSeating;
        for ($row = 0; $row < $height; $row += 1) {
            for ($column = 0; $column < $width; $column += 1) {
                $occupied = call_user_func($functionName, $seating, $row, $column);
                $currentSeat = $seating[$row][$column];

                if ($occupied === 0 && $currentSeat === 'L') {
                    $previousSeating[$row][$column] = '#';
                }
                else if ($occupied >= $tolerance && $currentSeat === '#') {
                    $previousSeating[$row][$column] = 'L';
                }
            }
        }
    }

    return array_sum(
        array_map(
            function(array $row) {
                $count = array_count_values($row);
                return $count['#'];
            },
            $seating
        )
    );
}

function amountOccupiedNeighbours(array $grid, int $row, int $column) {
    $occupied = 0;
    $neighbours = [
        [-1, -1], [-1, -0], [-1,  1],
        [ 0, -1],           [ 0,  1],
        [ 1, -1], [ 1,  0], [ 1,  1],
    ];
    foreach ($neighbours as $neighbour) {
        $x = $row + $neighbour[1];
        $y = $column + $neighbour[0];
        if (isset($grid[$x]) && isset($grid[$x][$y])) {
            $occupied += $grid[$x][$y] === '#' ? 1 : 0;
        }
    }
    return $occupied;
}

function amountDirectionalNeighbours(array $grid, int $row, int $column) {
    $occupied = 0;
    $directions = [
        [-1, -1], [-1, -0], [-1,  1],
        [ 0, -1],           [ 0,  1],
        [ 1, -1], [ 1,  0], [ 1,  1],
    ];
    foreach ($directions as $direction) {
        $distance = 1;
        while (true) {
            $x = $row + $direction[1] * $distance;
            $y = $column + $direction[0] * $distance;
            if (! isset($grid[$x]) || ! isset($grid[$x][$y]) || $grid[$x][$y] === 'L') {
                break;
            }
            else if ($grid[$x][$y] === '#') {
                $occupied += 1;
                break;
            }
            $distance += 1;
        }
        $x = $row + $direction[1] * $distance;
        $y = $column + $direction[0] * $distance;
    }
    return $occupied;
}
