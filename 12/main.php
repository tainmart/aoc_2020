<?php

$inputs = array_map(
    function($line) {
        $line = trim($line);
        return [substr($line, 0, 1), (int)substr($line, 1)];
    }, 
    file('input.txt')
);

$directions = [
    'N' => [ 0, 1],
    'E' => [ 1, 0],
    'S' => [ 0,-1],
    'W' => [-1, 0],
];
$turnings = [
      0 => 'N',
     90 => 'E',
    180 => 'S',
    270 => 'W',
];
$facing = 90;

$solution1 = array_reduce(
    $inputs,
    function($result, $line) use ($directions, $turnings, &$facing) {
        [$action, $value] = $line;
        if (array_key_exists($action, $directions)) {
            return [
                $result[0] + $value * $directions[$action][0],
                $result[1] + $value * $directions[$action][1],
            ];
        }
        if ($action === 'L' || $action === 'R') {
            $turn = $action === 'R' ? $value : 360 - $value;
            $facing = ($facing + $turn) % 360;
        }
        if ($action === 'F') {
            $current_direction = $turnings[$facing];
            return [
                $result[0] + $value * $directions[$current_direction][0],
                $result[1] + $value * $directions[$current_direction][1],
            ];
        }
        return $result;
    },
    [0,0]
);
$solution1=abs($solution1[0]) + abs($solution1[1]);

$waypoint = [10, 1];
$solution2 = array_reduce(
    $inputs,
    function($result, $line) use ($directions, &$waypoint) {
        [$action, $value] = $line;
        if (array_key_exists($action, $directions)) {
            $waypoint = [
                $waypoint[0] + $value * $directions[$action][0],
                $waypoint[1] + $value * $directions[$action][1],
            ];
            return $result;
        }
        if ($action === 'L' || $action === 'R') {
            $turn = $action === 'R' ? $value : 360 - $value;
            if ($turn === 90) {
                $waypoint = [$waypoint[1], -$waypoint[0]];
            } 
            else if ($turn === 180) {
                $waypoint = [-$waypoint[0], -$waypoint[1]];
            }
            else if ($turn === 270) {
                $waypoint = [-$waypoint[1], $waypoint[0]];
            }
            return $result;
        }
        if ($action === 'F') {
            return [
                $result[0] + $value * $waypoint[0],
                $result[1] + $value * $waypoint[1],
            ];
        }
        return $result;
    },
    [0,0]
);
$solution2=abs($solution2[0]) + abs($solution2[1]);

echo "Solution Day 12-1: $solution1\n";
echo "Solution Day 12-2: $solution2\n";

