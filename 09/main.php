<?php

$inputs = array_map('intval', file('input.txt'));
$solution1 = 0;
$solution2 = 0;

$preamble=25;
for ($index = $preamble; $index < count($inputs); $index += 1) {
    if (! inSumOfPairs($inputs[$index], array_slice($inputs, $index - $preamble, $preamble))) {
        $solution1 = $inputs[$index];
        break;
    }
}
$solution2 = getEncryptionWeakness($solution1, $inputs);

echo "Solution Day 09-1: $solution1\n";
echo "Solution Day 09-2: $solution2\n";

function inSumOfPairs(int $result, array $numbers): bool {
    $length = count($numbers);
    for ($first_index = 0; $first_index < $length; $first_index += 1) {
        for ($second_index = $first_index + 1; $second_index < $length; $second_index += 1) {
            $sum = $numbers[$first_index] + $numbers[$second_index];
            if ($sum === $result) {
                return true;
            }
        }
    }
    return false;
}

function getEncryptionWeakness(int $result, array $numbers): ?int {
    $length = count($numbers);
    for ($pos = 0; $pos < $length; $pos += 1) {
        for($contiguousLength = 2; $contiguousLength < $length - $pos; $contiguousLength += 1) {
            $contiguous = array_slice($numbers, $pos, $contiguousLength);
            $sum = array_sum($contiguous);
            if ($sum === $result) {
                return min($contiguous) + max($contiguous);
            }
            if ($sum > $result) {
                break;
            }
        }
    }
    return null;
}
